/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mongkol.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DIY
 */
public class UpdateCompany {
  public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            //update
            stmt.executeUpdate("UPDATE COMPANY set SALARY = 25000.00 where ID=1");
            conn.commit();
            //select
            ResultSet rs = stmt.executeQuery("SELECT * FROM COMPANY");
            
            while(rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                String address =rs.getString("address");
                float salary = rs.getFloat("salary");
                System.out.println("ID = "+id);
                System.out.println("NAME = "+name);
                System.out.println("AGE = "+age);
                System.out.println("ADDRESS = "+address);
                System.out.println("ID = "+salary);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
      
}
